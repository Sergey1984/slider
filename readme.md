COMMAND LINE:

	1. npm install
	2. npm start (development) || npm run production (build)

For disable ES6 support remove:

	1. babel-preset-es2015 and babelify from package.json devDependencies
	2. "// ES6 Support for Browserify" from gulpfile.js
