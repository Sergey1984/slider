"use strict";

const timeout = 2000;

class Carousel {
    constructor(id) {
        this.elements = {};
        this.colors = {};
        this.currentCount = 0;

        this.setElements(id);
        this.setColours(id);
    }

    setElements(id) {
        const el = document.getElementById(id);

        this.elements.wrapper = el.querySelector("._wrapper");
        this.elements.slides = el.querySelector("._slides");
        this.elements.items = el.querySelectorAll("._item");
    }

    defineItemsPositions(count) {
        const { items, slides } = this.elements;
        const averageDeg = 360 / items.length;

        items.forEach((item, index) => {
            const content = item.querySelector("._content");
            const deg = averageDeg * (index + count);

            item.style.transform = "rotate(" + deg + "deg)";
            content.style.transform = "rotate(" + -deg + "deg)";

            // item.style.transition = count === 0 ? "none" : "";
        });
    }

    updateCounter() {
        if (this.currentCount + 1 === this.elements.items.length) {
            this.currentCount = 0;
        } else {
            this.currentCount++;
        }
    }

    setColours() {
        this.elements.items.forEach((item, index) => {
            const color = item.getAttribute("data-area-color");
            this.colors[index] = color;
            item.querySelector("._area").style.backgroundColor = color;
        });
    }

    loopPositions() {
        setInterval(() => {
            this.defineItemsPositions(this.currentCount);
            this.updateCounter();
        }, timeout);
    }

    init() {
        this.loopPositions();
    }
}

export default Carousel;